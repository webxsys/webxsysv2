<?php
/**
 * Created by PhpStorm.
 * User: goldenboy
 * Date: 10/27/2018
 * Time: 12:01 AM
 */
?>
<div class="module-small bg-dark">
          <div class="container">
            <div class="row">
              <div class="col-sm-3">
                <div class="widget">
                  <h5 class="widget-title font-alt">WEBXSYS</h5>
                  <p>We're here to help</p>
                  <p>Phone: +1 234 567 89 10</p>Fax: +1 234 567 89 10
                  <p>Email:<a href="#">&nbsp; info@webxsys.com</a></p>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="widget">
                  <h5 class="widget-title font-alt">Solutions</h5>
                  <ul class="icon-list">
                    <li><a href="#">FREE Website Mock-up</a></li>
                    <li><a href="#">Development</a></li>
                    <li><a href="#">Design</a></li>
                    <li><a href="#">Analysis</a></li>
                    <li><a href="#">Maintenance</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="widget">
                  <h5 class="widget-title font-alt">Blog Categories</h5>
                  <ul class="icon-list">
                    <li><a href="#">Technology</a></li>
                    <li><a href="#">Web Development</a></li>
                    <li><a href="#">Website Design</a></li>
                    <li><a href="#">Cloud Computing</a></li>

                  </ul>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="widget">
                  <h5 class="widget-title font-alt">Recent Posts</h5>
                  <ul class="widget-posts">
                    <li class="clearfix">
                      <div class="widget-posts-image"><a href="#"><img src="assets/images/rp-1.jpg" alt="Post Thumbnail"/></a></div>
                      <div class="widget-posts-body">
                        <div class="widget-posts-title"><a href="#">Route 53 - AWS Cloud Managed DNS</a></div>
                        <div class="widget-posts-meta">23 january</div>
                      </div>
                    </li>
                    <li class="clearfix">
                      <div class="widget-posts-image"><a href="#"><img src="assets/images/rp-2.jpg" alt="Post Thumbnail"/></a></div>
                      <div class="widget-posts-body">
                        <div class="widget-posts-title"><a href="#">Cross Site Request Forgery Defensive Tactics</a></div>
                        <div class="widget-posts-meta">15 February</div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
