<?php
/**
 * Created by PhpStorm.
 * User: goldenboy
 * Date: 10/18/2018
 * Time: 12:19 AM
 */
?>
<nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse">
                <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span>
                <span class="icon-bar"></span><span class="icon-bar"></span></button>
            <a class="navbar-brand" href="index.php"><img src="assets/images/logo.png" style="margin-top:-5px;width:160px;height:30px;"></a>
        </div>
        <div class="collapse navbar-collapse" id="custom-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php" >About</a></li>
                <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Development</a>
                    <ul class="dropdown-menu">
                        <li><a href="web-development.php">Web Development</a></li>
                        <li><a href="ecommerce.php.php">E-Commerce</a></li>
                        <li><a href="customer-software.php">Custom Software</a></li>
                        <li><a href="website-maintenance.php">Website Maintenance</a></li>
                        <li><a href="mobile-development.php">Mobile Development</a></li>
                        <li><a href="web-services-development.php">Web Services Development</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Design</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="website-design.php">Website Design</a></li>
                    </ul>
                </li>
                <li><a href="portfolio.php">Work</a></li>
                <li><a href="blog.php">Blog</a></li>
                <li><a href="contact.php" >Contact</a></li>
            </ul>
        </div>
    </div>
</nav>

